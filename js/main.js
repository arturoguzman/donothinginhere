import { words } from "./words.js";

let trigger;
let target;

if (document.location.pathname === "/index_withstyle.html") {
  trigger = document.getElementById("send-button");
  target = document.getElementById("result");
} else {
  trigger = target = document.getElementById("sentence");
}

const randSentence = () => {
  const randomise = words[Math.floor(Math.random() * words.length)];
  target.innerHTML = randomise;
};

window.addEventListener("load", () => {
  randSentence();
});
trigger.addEventListener("click", () => {
  randSentence();
});
